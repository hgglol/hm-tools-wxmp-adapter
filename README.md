# hm-tools-wxmp-adapter

[![996.icu](https://img.shields.io/badge/link-996.icu-red.svg)](https://996.icu)

#### 介绍
对微信公众号API接口的封装，官方API文档地址：[https://mp.weixin.qq.com/wiki](https://mp.weixin.qq.com/wiki)  
**封装微信接口过程是极度辛苦的，因为腾讯的文档总是写的不明不白，客服也总是联系不上！~**

#### 软件架构
软件架构说明

- 主要流程：
![如何使用快照版本](/documents/主体流程示例图.png)

#### 功能特性

1. 获取到access_token后，会序列化存储到本地，每次请求微信接口时，都会先尝试取本地缓存的access_token，并校验其时间是否过期，如果过期了，则重新获取并序列化缓存到本地。
2. 如果因为某种原因，请求微信接口时其access_token过期或者其它原因失效，程序会自动刷新一次本地缓存的access_token，并尝试一次重新请求微信接口。
3. 使用了Apache httpclient连接池。

#### 使用说明

1. 引用jar包，因还在编写中，暂时只提供快照版本。
```
<dependency>
  <groupId>top.hmtools</groupId>
  <artifactId>hm-tools-wxmp-adapter</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```
2. 初始化配置信息对象实例
```
WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
wxmpConfiguration.setAppid(appid)  //必须，设置appid
		.setAppsecret(appsecret)	//必须，设置appsecret
		.setCharset("utf-8")	//可选，设置字符编码，缺省是“utf-8”
		.seteUrlServer(EUrlServer.api)	//可选，设置微信公众号服务器地址，缺省是：api.weixin.qq.com
		.saveToThreadLocal();		//必须，将配置好的对象实例存入ThreadLocal，这样就可以不用显示的传参了
```
3. 执行接口调用（以创建用户标签为例）
```
		UserTagsTools userTagsTools = new UserTagsTools();
		TagsBean tagsBean = new TagsBean();
		tagsBean.setName("神奇码农cc");
		
		TagsParamBean tagsParamBean = new TagsParamBean();
		tagsParamBean.setTag(tagsBean);
		TagsBean tagsBean2 = userTagsTools.createTags(tagsParamBean);
		
```


#### 如何使用本组件的快照版本

1. 在自己的私服建立一个快照代理仓库即可。https://oss.sonatype.org/content/repositories/snapshots/
![如何使用快照版本](/images/howToUseSnapshot.jpg)

2. 将建好的参考纳入公共库
![如何使用快照版本](/images/howToUseSnapshot-2.jpg)
