package top.hmtools.wxmp.user;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.accessToken.AccessTokenToolsTest;

public class UserTagsToolsTest extends AccessTokenToolsTest{
	
	final Logger logger = LoggerFactory.getLogger(UserTagsToolsTest.class);

	@Test
	public void testcreateTags(){
		this.logger.info("测试创建新的标签开始……");
		UserTagsTools userTagsTools = new UserTagsTools();
		TagsBean tagsBean = new TagsBean();
		tagsBean.setName("神奇码农cc");
		
		TagsParamBean tagsParamBean = new TagsParamBean();
		tagsParamBean.setTag(tagsBean);
		TagsBean tagsBean2 = userTagsTools.createTags(tagsParamBean);
		
		this.logger.info("创建标签结果：{}",tagsBean2);
		this.logger.info("测试创建新的标签结束……");
	}
}
