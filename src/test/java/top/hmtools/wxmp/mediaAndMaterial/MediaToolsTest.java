package top.hmtools.wxmp.mediaAndMaterial;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import top.hmtools.wxmp.UrlServer.EUrlServer;
import top.hmtools.wxmp.accessToken.AccessTokenTools;
import top.hmtools.wxmp.accessToken.AccessTokenToolsTest;
import top.hmtools.wxmp.configuration.ThreadLocalHander;

/**
 * 临时素材接口相关
 * @author Hybomyth
 *
 */
public class MediaToolsTest extends AccessTokenToolsTest{

	/**
	 * 测试上传临时素材接口
	 */
	@Test
	public void testUploadMedia(){
		this.logger.info("测试上传临时素材开始》》》");
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("images/1.jpg");
		MediaTools mediaTools = new MediaTools();
		MediaBean uploadMedia = mediaTools.uploadMedia("111.jpg", inputStream, EMediaType.image);
		this.logger.info("获取的反馈结果是：{}",uploadMedia);
		this.logger.info("测试上传临时素材结束《《《");
	}
	
	/**
	 * 直接测试微信公众号上传临时素材接口
	 * <br>最简洁 输入流 上传到微信服务器
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@Test
	public void testuploadMeidaCC() throws ClientProtocolException, IOException{
		//获取 access token
		AccessTokenTools aaa =AccessTokenTools.getInstance(null);
		
		//获取要上传的文件
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("images/1.jpg");
		
		//设置post请求
		HttpPost post = new HttpPost("https://api.weixin.qq.com/cgi-bin/media/upload?access_token="+aaa.getAccessTokenInfoBean(ThreadLocalHander.getWxmpConfiguration().getAppid(), ThreadLocalHander.getWxmpConfiguration().getAppsecret()).getAccess_token()+"&type=image");
		ByteArrayBody byteArrayBody = new ByteArrayBody(IOUtils.toByteArray(inputStream), System.currentTimeMillis()+"_22.jpeg");
		MultipartEntityBuilder meb = MultipartEntityBuilder.create();
		meb.addPart("media",byteArrayBody);
		HttpEntity entity = meb.build();
		post.setEntity(entity);
		
		//执行HTTP post请求，并打印微信服务器反馈的数据信息
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpResponse response = httpclient.execute(post);
		entity = response.getEntity();
		String result = EntityUtils.toString(entity, "utf-8");
		EntityUtils.consume(entity);// 关闭流
		System.out.println(result);
	}
	
	/**
	 * 直接测试微信公众号上传临时素材接口
	 * <br>最简洁 文件 上传到微信服务器
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@Test
	public void testuploadMeidaBB() throws ClientProtocolException, IOException{
		//获取 access token
		AccessTokenTools aaa = AccessTokenTools.getInstance(null);
		
		//获取要上传的文件
		URL resource = Thread.currentThread().getContextClassLoader().getResource("images/1.jpg");
		File file = new File(resource.getFile());
		
		//设置post请求
		String access_token = aaa.getAccessTokenInfoBean(ThreadLocalHander.getWxmpConfiguration().getAppid(), ThreadLocalHander.getWxmpConfiguration().getAppsecret()).getAccess_token();
		this.logger.info("access_token：{}",access_token);
		HttpPost post = new HttpPost("https://api.weixin.qq.com/cgi-bin/media/upload?access_token="+access_token+"&type=image");
		MultipartEntityBuilder meb = MultipartEntityBuilder.create();
		meb.addBinaryBody("media", file);
		
		HttpEntity entity = meb.build();
		post.setEntity(entity);
		
		//执行HTTP post请求，并打印微信服务器反馈的数据信息
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpResponse response = httpclient.execute(post);
		entity = response.getEntity();
		String result = EntityUtils.toString(entity, "utf-8");
		EntityUtils.consume(entity);// 关闭流
		System.out.println(result);
	}
	
	/**
	 * 直接测试微信公众号上传临时素材接口
	 */
	@Test
	public void testuploadMediaAA() throws ClientProtocolException, IOException{
		AccessTokenTools aaa = AccessTokenTools.getInstance(null);
		
		HttpPost post = new HttpPost("https://api.weixin.qq.com/cgi-bin/media/upload?access_token="+aaa.getAccessTokenInfoBean(ThreadLocalHander.getWxmpConfiguration().getAppid(), ThreadLocalHander.getWxmpConfiguration().getAppsecret()).getAccess_token()+"&type=image");
		
		URL resource = Thread.currentThread().getContextClassLoader().getResource("images/1.jpg");
		File file = new File(resource.getFile());
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpEntity entity = null;
		HttpResponse response = null;
		String BoundaryStr = "------------7da2e536604c8";
		post.addHeader("Connection", "keep-alive");
		post.addHeader("Accept", "*/*");
		post.addHeader("Content-Type", "multipart/form-data;boundary=" + BoundaryStr);
		post.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
		MultipartEntityBuilder meb = MultipartEntityBuilder.create();
		meb.setBoundary(BoundaryStr)
		.setCharset(Charset.forName("utf-8"))
		.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		meb.addBinaryBody("media", file, ContentType.APPLICATION_OCTET_STREAM, file.getName());
		entity = meb.build();
		post.setEntity(entity);
		response = httpclient.execute(post);
		entity = response.getEntity();
		String result = EntityUtils.toString(entity, "utf-8");
		EntityUtils.consume(entity);// 关闭流
		System.out.println(result);
	}
}
