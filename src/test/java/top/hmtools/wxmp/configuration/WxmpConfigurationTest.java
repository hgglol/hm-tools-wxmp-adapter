package top.hmtools.wxmp.configuration;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.accessToken.AppId;

public class WxmpConfigurationTest {

	private final Logger logger = LoggerFactory.getLogger(WxmpConfigurationTest.class);
	
	/**
	 * 测试将配置信息对象实例写入 threadLocal
	 */
	@Test
	public void testSaveToThreadLocal(){
		WxmpConfiguration config = new WxmpConfiguration();
		config.setAppid(AppId.appid).setAppsecret(AppId.appsecret).saveToThreadLocal();
		this.logger.info("完成写入threadLocal：{}",config);
		
		//尝试读取
		WxmpConfiguration wxmpConfiguration = ThreadLocalHander.getWxmpConfiguration();
		this.logger.info("从threadLocal获取配置信息：{}",wxmpConfiguration);
	}
}
