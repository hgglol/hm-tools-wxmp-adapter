package top.hmtools.wxmp.accessToken;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.UrlServer.EUrlServer;
import top.hmtools.wxmp.configuration.ThreadLocalHander;
import top.hmtools.wxmp.configuration.WxmpConfiguration;

/**
 * 测试 AccessTokenTools
 * @author Hybomyth
 *
 */
public class AccessTokenToolsTest {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 初始化配置信息对象实例
	 * <br>如果报错，请修改/hm-tools-wxmp-adapter/src/test/java/top/hmtools/wxmp/accessToken/AppId.java.txt文件的后缀名
	 */
	@Before
	public void initConfig(){
		this.logger.info("初始化化配置信息开始……");
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		//设置appid，appsecret并将对象实例存入threadLocal，这样就可以在其它代码块中优雅的获取数据。
		wxmpConfiguration.setAppid(AppId.appid).setAppsecret(AppId.appsecret).setCharset("utf-8").seteUrlServer(EUrlServer.api).saveToThreadLocal();
		this.logger.info("配置信息原文是：{}",wxmpConfiguration);
		this.logger.info("初始化化配置信息成功结束……");
	}

	/**
	 * 测试获取access Token
	 */
	@Test
	public void testGetAccessToken(){
		this.logger.info("测试获取access Token 开始。。。");
//		AccessTokenTools accessTokenTools = new AccessTokenTools(EUrlServer.api);
		AccessTokenTools accessTokenTools = AccessTokenTools.getInstance(EUrlServer.api);
		ATBean atBean = accessTokenTools.getAccessTokenInfoBean(ThreadLocalHander.getWxmpConfiguration().getAppid(), ThreadLocalHander.getWxmpConfiguration().getAppsecret());
		
		this.logger.info("测试获取access Token 成功结束。。。{}",atBean);
	}
	
	/**
	 * 测试刷新本地access Token
	 */
	@Test
	public void testGetRefreshedAccessToken(){
		//请移步至 top.hmtools.wxmp.RefreshAccessTokenTest
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
