package top.hmtools.wxmp;

import org.junit.Test;

import top.hmtools.wxmp.accessToken.AccessTokenTools;
import top.hmtools.wxmp.accessToken.AccessTokenToolsTest;
import top.hmtools.wxmp.httpclient.HttpAdapter;
import top.hmtools.wxmp.menu.MenuToolsTest;

public class RefreshAccessTokenTest extends AccessTokenToolsTest{

	/**
	 * 测试刷新本地access Token
	 */
	@Test
	public void testGetRefreshedAccessToken(){
		this.logger.info("测试刷新access Token 开始。。。");
		
		MenuToolsTest menuToolsTest = new MenuToolsTest();

		this.logger.info("执行一次创建自定义菜单");
		menuToolsTest.testcreateMenu();
		
		this.logger.info("手动将accessToken设置成无效值");
		AccessTokenTools accessTokenTools = HttpAdapter.getAccessTokenTools();
		accessTokenTools.setAccessToken("123456");
		
//		this.logger.info("再执行  获取access Token，但是并不缓存到本地，不过微信服务器侧已经更新了access Token");
//		ATBean atBean = accessTokenTools.getATbeanFromWXOnly(ThreadLocalHander.getWxmpConfiguration().getAppid(), ThreadLocalHander.getWxmpConfiguration().getAppsecret());
//		this.logger.info("执行的结果是：{}",atBean);
		
//		this.logger.info("休眠5分钟，使度过微信服务器处前后两accessToken并存可用期间");
//		try {
//			Thread.sleep(1000*60*5);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		this.logger.info("再执行一次创建自定义菜单");
		menuToolsTest.testcreateMenu();
		
		this.logger.info("测试刷新access Token 成功结束。。。");
	}
}
