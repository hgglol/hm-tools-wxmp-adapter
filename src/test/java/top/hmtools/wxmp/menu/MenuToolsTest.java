package top.hmtools.wxmp.menu;

import org.junit.Test;

import top.hmtools.wxmp.accessToken.AccessTokenToolsTest;

public class MenuToolsTest extends AccessTokenToolsTest{

	@Test
	public void testcreateMenu(){
		this.logger.info("测试创建自定义菜单开始……");
		
		String baseUrl = "wx.hubeihome.net";
		
		//底部第一个主按钮
		ButtonBean bbGuanwang = new ButtonBean();
		bbGuanwang.setName("官网").setType("view").setUrl("http://m.hubeihome.net/main/index/index.html");
		
		ButtonBean bbDongTai = new ButtonBean();
		bbDongTai.setName("动态").setType("view").setUrl("http://"+baseUrl+"/main/news/index.html");
		
		ButtonBean bbVideo = new ButtonBean();
		bbVideo.setName("视频").setType("view").setUrl("http://"+baseUrl+"/main/news/video.html");
		
		ButtonBean buttonBeanIndex = new ButtonBean();
		buttonBeanIndex.setName("家园体验");
		buttonBeanIndex.addSubButton(bbGuanwang,bbDongTai);
		
		//底部第二个主按钮
		ButtonBean buttonBeanYuYue = new ButtonBean();
		buttonBeanYuYue.setName("服务预约");
		
		ButtonBean bbHuiYi = new ButtonBean();
		bbHuiYi.setName("会议会展").setType("view").setUrl("http://"+baseUrl+"/main/reservation/exhibition.html");
		
		ButtonBean bbCanYin = new ButtonBean();
		bbCanYin.setName("餐饮").setType("view").setUrl("http://"+baseUrl+"/main/reservation/food.html");
		
		ButtonBean bbDuJia = new ButtonBean();
		bbDuJia.setName("家园参观").setType("view").setUrl("http://"+baseUrl+"/main/reservation/rural.html");
		
		ButtonBean bbBiaoYan = new ButtonBean();
		bbBiaoYan.setName("文艺表演").setType("view").setUrl("http://"+baseUrl+"/main/reservation/show.html");
		
		ButtonBean bbGengDuo = new ButtonBean();
		bbGengDuo.setName("更多").setType("view").setUrl("http://"+baseUrl+"/main/reservation/index.html");
		
		buttonBeanYuYue.addSubButton(bbHuiYi,bbCanYin,bbDuJia,bbBiaoYan,bbGengDuo);
		
		//底部第三个主按钮
		ButtonBean buttonBeanWoDe = new ButtonBean();
		buttonBeanWoDe.setName("我的");
		buttonBeanWoDe.setUrl("http://"+baseUrl+"/main/personal/index.html");
		buttonBeanWoDe.setType("view");
		
		
		MenuBean menuBean = new MenuBean();
		menuBean.addButton(buttonBeanIndex,buttonBeanYuYue,buttonBeanWoDe);
		this.logger.info("组装好请求参数数据结构完毕：{}",menuBean);
		
		MenuTools menuTools = new MenuTools();
		boolean isCreatedMenu = menuTools.createMenu(menuBean);
		if(isCreatedMenu){
			this.logger.info("测试创建自定义菜单成功结束……");
		}else{
			this.logger.error("测试创建自定义菜单失败结束……");
		}
	}
	
	
	@Test
	public void testGetMenu(){
		this.logger.info("测试获取自定义菜单接口开始……");
		MenuTools menuTools = new MenuTools();
		MenuBean menu = menuTools.getMenu();
		this.logger.info("获取到的自定义菜单：{}",menu);
		this.logger.info("测试获取自定义菜单接口结束……");
	}
	
	@Test
	public void testDeleteMenu(){
		this.logger.info("测试删除自定义菜单接口开始……");
		this.testGetMenu();
		MenuTools menuTools = new MenuTools();
		menuTools.deleteMenu();
		this.testGetMenu();
		this.logger.info("测试删除自定义菜单接口结束……");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
