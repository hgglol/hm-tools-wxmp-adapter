package top.hmtools.threadlocal;

/**
 * 用于优雅读写配置信息的threadLocal工具
 * @author Hybomyth
 *
 */
public class ThreadLocalHanderDemo {

	private static ThreadLocal<String> wxmpConfigTL = new ThreadLocal<>();
	
	/**
	 * 设置配置信息到threadLocal
	 * @param wxmpConfiguration
	 */
	public static void setStr(String str){
		wxmpConfigTL.set(str);
	}
	
	/**
	 * 从threadLocal中获取配置信息
	 * @return
	 */
	public static String getStr(){
		return wxmpConfigTL.get();
	}
}
