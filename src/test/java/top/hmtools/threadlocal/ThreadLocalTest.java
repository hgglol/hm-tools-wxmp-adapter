package top.hmtools.threadlocal;

import org.junit.Test;

/**
 * 对jdk中ThreadLocal的测试实验
 * @author HyboJ
 *
 */
public class ThreadLocalTest {

	/**
	 * 测试多线程情况下使用 ThreadLocal
	 */
	@Test
	public void testMultiThreadUseThreadLocal(){
		ThreadLocalHanderDemo.setStr("In factor,i am threadLocalHanderDemo");
		ThreadDemo td = new ThreadDemo();
		
		Thread tt_1 = new Thread(td);
		Thread tt_2 = new Thread(td);
		Thread tt_3 = new Thread(td);
		Thread tt_4 = new Thread(td);
		Thread tt_5 = new Thread(td);
		Thread tt_6 = new Thread(td);
		
		tt_1.start();
		tt_2.start();
		tt_3.start();
		tt_4.start();
		tt_5.start();
		tt_6.start();
		
		Car car = new Car();
		car.say();
	}
}
