package top.hmtools.threadlocal;

public class ThreadDemo implements Runnable{

	@Override
	public void run() {
		Car car = new Car();
		car.say();
		
		Bus bus = new Bus();
		bus.say();
	}

}
