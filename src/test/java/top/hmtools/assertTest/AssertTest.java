package top.hmtools.assertTest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AssertTest {
	
	private final Logger logger = LoggerFactory.getLogger(AssertTest.class);

	/**
	 * 须显示开启-ea开关，执行程序：
		<br>例如：C:\>java -ea AssertFoo
	 */
//	@Test
	public void testAssert(){
		assert true;
		this.logger.info("assert true is run ok");
		assert  false:"assert false";
		this.logger.info("assert false is run ok");
	}
}
