package top.hmtools.wxmp.UrlServer;

/**
 * 微信服务器URL枚举
 * @author Hybomyth
 *
 */
public enum EUrlServer {

	/**
	 * 通用域名(api.weixin.qq.com)，使用该域名将访问官方指定就近的接入点；
	 */
	api("api.weixin.qq.com"),
	/**
	 * 上海域名(sh.api.weixin.qq.com)，使用该域名将访问上海的接入点；
	 */
	sh("sh.api.weixin.qq.com"),
	/**
	 * 深圳域名(sz.api.weixin.qq.com)，使用该域名将访问深圳的接入点；
	 */
	sz("sz.api.weixin.qq.com"),
	/**
	 * 香港域名(hk.api.weixin.qq.com)，使用该域名将访问香港的接入点。
	 */
	hk("hk.api.weixin.qq.com");
	
	private String domain;
	
	EUrlServer(String domain) {
		this.domain=domain;
	}
	
	@Override
	public String toString() {
		return this.domain;
	}
}
