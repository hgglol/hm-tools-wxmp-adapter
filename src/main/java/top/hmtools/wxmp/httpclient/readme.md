#### 本工具包统一的HTTP客户端请求工具
- 集成统一获取access token，如果第一次请求数据时微信服务器反馈access token失效，会有一次刷新本地 access token，然后再尝试请求一次。
- 统一请求参数组装，返回响应数据反序列化成指定类对象实例。
- 主工具类：top.hmtools.wxmp.httpclient.HttpAdapter