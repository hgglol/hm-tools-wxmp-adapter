package top.hmtools.wxmp.mediaAndMaterial;

import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.httpclient.HttpAdapter;

/**
 * 临时素材操作工具
 * @author HyboJ
 *
 */
public class MediaTools {

	final Logger logger = LoggerFactory.getLogger(MediaTools.class);
	
	/**
	 * 新增临时素材URI
	 */
	private String uploadMedia = "cgi-bin/media/upload";
	
	/**
	 * 上传临时素材
	 * @param fileName	媒体名称
	 * @param inputStream	输入流
	 * @param mediaType	媒体类型
	 * @return
	 */
	public MediaBean uploadMedia(String fileName,InputStream inputStream, EMediaType mediaType){
		MediaBean mediaBean = HttpAdapter.doPostStream(this.uploadMedia, "media", fileName, inputStream,mediaType,MediaBean.class);
		return mediaBean;
	}
}
