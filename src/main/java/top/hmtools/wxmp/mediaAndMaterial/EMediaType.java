package top.hmtools.wxmp.mediaAndMaterial;

/**
 * 临时素材类型
 * @author HyboJ
 *
 */
public enum EMediaType {

	/**
	 * 图片
	 */
	image("image"),
	
	/**
	 * 语音
	 */
	voice("voice"),
	
	/**
	 * 视频
	 */
	video("video"),
	
	/**
	 * 缩略图
	 */
	thumb("thumb");
	
	private String name;
	
	private EMediaType(String name) {
		this.name=name;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
