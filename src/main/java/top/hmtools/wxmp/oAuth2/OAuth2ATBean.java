package top.hmtools.wxmp.oAuth2;

import java.io.Serializable;

/**
 * oauth2微信授权过程中获取的access_token对象实例，这是另外一种含义的access_token
 * @author Hybomyth
 *
 */
public class OAuth2ATBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5579221967546564633L;

	private String access_token;
	
	private int expires_in;
	
	private String refresh_token;
	
	private String openid;
	
	private String scope;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "OAuth2ATBean [access_token=" + access_token + ", expires_in=" + expires_in + ", refresh_token="
				+ refresh_token + ", openid=" + openid + ", scope=" + scope + "]";
	}
	
	
}
