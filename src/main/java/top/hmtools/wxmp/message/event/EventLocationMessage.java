package top.hmtools.wxmp.message.event;

import top.hmtools.wxmp.message.BaseMessage;

/**
 * 用户地理位置事件消息
 * @author Hybomyth
 *
 */
public class EventLocationMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = -279750089825821519L;

	/**
	 * 事件类型，LOCATION
	 */
	private String Event;
	
	/**
	 * 地理位置纬度
	 */
	private String Latitude;
	
	/**
	 * 地理位置经度
	 */
	private String Longitude;
	
	/**
	 * 地理位置精度
	 */
	private String Precision;

	public String getEvent() {
		return Event;
	}

	public void setEvent(String event) {
		Event = event;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getPrecision() {
		return Precision;
	}

	public void setPrecision(String precision) {
		Precision = precision;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "EventLocationMessage [Event=" + Event + ", Latitude=" + Latitude + ", Longitude=" + Longitude
				+ ", Precision=" + Precision + ", ToUserName=" + ToUserName + ", FromUserName=" + FromUserName
				+ ", CreateTime=" + CreateTime + ", MsgType=" + MsgType + ", MsgId=" + MsgId + "]";
	}
	
	
}
