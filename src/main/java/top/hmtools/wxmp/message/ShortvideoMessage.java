package top.hmtools.wxmp.message;
/**
 * 小视频消息
 * @author Hybomyth
 *
 */
public class ShortvideoMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -453338811135241536L;

	/**
	 * 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	private String MediaId;
	
	/**
	 * 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	private String ThumbMediaId;

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getThumbMediaId() {
		return ThumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		ThumbMediaId = thumbMediaId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ShortvideoMessage [MediaId=" + MediaId + ", ThumbMediaId=" + ThumbMediaId + ", ToUserName=" + ToUserName
				+ ", FromUserName=" + FromUserName + ", CreateTime=" + CreateTime + ", MsgType=" + MsgType + ", MsgId="
				+ MsgId + "]";
	}
	
	
}
