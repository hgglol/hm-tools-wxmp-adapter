package top.hmtools.wxmp.message;

/**
 * 文本消息
 * @author Hybomyth
 *
 */
public class TextMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8090390109332033707L;
	
	/**
	 * 文本消息内容
	 */
	private String Content;

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "TextMessage [Content=" + Content + ", ToUserName=" + ToUserName + ", FromUserName=" + FromUserName
				+ ", CreateTime=" + CreateTime + ", MsgType=" + MsgType + ", MsgId=" + MsgId + "]";
	}
	
	

}
