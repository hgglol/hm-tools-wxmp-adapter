package top.hmtools.wxmp.message;

import java.io.Serializable;

public abstract class BaseMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3336594454553059603L;
	/**
	 * 接收方微信号
	 */
	protected String ToUserName;
	/**
	 * 发送方微信号，若为普通用户，则是一个OpenID
	 */
	protected String FromUserName;
	/**
	 * 消息创建时间
	 */
	protected int CreateTime;
	/**
	 * 消息类型
	 */
	protected eMsgType MsgType;
	/**
	 * 消息id，64位整型
	 */
	protected String MsgId;
	
	
	
	public String getToUserName() {
		return ToUserName;
	}



	public void setToUserName(String toUserName) {
		ToUserName = toUserName;
	}



	public String getFromUserName() {
		return FromUserName;
	}



	public void setFromUserName(String fromUserName) {
		FromUserName = fromUserName;
	}



	public int getCreateTime() {
		return CreateTime;
	}



	public void setCreateTime(int createTime) {
		CreateTime = createTime;
	}



	public String getMsgType() {
		return MsgType.getName();
	}


	public void setMsgType(String msgType) {
		if(msgType==null||msgType.length()<1){
			return;
		}
		
		eMsgType[] values = eMsgType.values();
		for(eMsgType ee:values){
			if(ee.getName().equals(msgType)){
				this.MsgType=ee;
			}
		}
	}
	
	public eMsgType getEMsgType(){
		return this.MsgType;
	}
	
	public void setEMsgType(eMsgType msgType){
		this.MsgType=msgType;
	}



	public String getMsgId() {
		return MsgId;
	}



	public void setMsgId(String msgId) {
		MsgId = msgId;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public enum eEventType{
		/**
		 * 订阅，以及扫码订阅
		 */
		subscribe(1,"subscribe"),
		/**
		 * 取消订阅
		 */
		unsubscribe(2,"unsubscribe"),
		/**
		 * 扫码（已关注）
		 */
		SCAN(3,"SCAN"),
		/**
		 * 上报地理位置
		 */
		LOCATION(4,"LOCATION"),
		/**
		 * 菜单事件，点击菜单拉取消息时的事件推送
		 */
		CLICK(5,"CLICK"),
		/**
		 * 菜单事件，点击菜单跳转链接时的事件推送
		 */
		VIEW(6,"VIEW")
		;
		
		private int code;
		
		private String name;
		
		private eEventType(int code,String name) {
			this.code=code;
			this.name=name;
		}

		public int getCode() {
			return code;
		}

		public String getName() {
			return name;
		}
		
		/**
		 * 根据名称获取code
		 * @param name
		 * @return
		 */
		public static int getCode(String name){
			eEventType[] values = eEventType.values();
			for(eEventType ee:values){
				if(ee.name.equalsIgnoreCase(name.trim())){
					return ee.code;
				}
			}
			return -1;
		}
	}

	/**
	 * 消息类型枚举
	 * @author Hybomyth
	 *
	 */
	public enum eMsgType{
		/**
		 * 文本消息
		 */
		text(1,"text"),
		/**
		 * 图片消息
		 */
		image(2,"image"),
		/**
		 * 音频消息
		 */
		voice(3,"voice"),
		/**
		 * 视频消息
		 */
		video(4,"video"),
		/**
		 * 短（小）视频消息
		 */
		shortvideo(5,"shortvideo"),
		/**
		 * 地理位置消息
		 */
		location(6,"location"),
		/**
		 * 链接消息
		 */
		link(7,"link"),
		/**
		 * 事件消息
		 */
		event(8,"event");
		
		/**
		 * 代号
		 */
		private int code;
		/**
		 * 消息类型名称
		 */
		private String name;
		
		/**
		 * 构造函数
		 * @param code
		 * @param name
		 */
		private eMsgType(int code,String name) {
			this.code=code;
			this.name=name;
		}

		public int getCode() {
			return code;
		}

		public String getName() {
			return name;
		}
		
		/**
		 * 根据名称获取代号
		 * @param name
		 * @return
		 */
		public static int getCode(String name){
			eMsgType[] values = eMsgType.values();
			for(eMsgType ee:values){
				if(ee.name.equalsIgnoreCase(name.trim())){
					return ee.code;
				}
			}
			return -1;
		}
		
		@Override
		public String toString() {
			return String.valueOf(this.code);
		}
	}
}
