package top.hmtools.wxmp.message;

public class LinkMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7329659443556249327L;

	/**
	 * 消息标题
	 */
	private String Title;
	
	/**
	 * 消息描述
	 */
	private String Description;
	
	/**
	 * 消息链接
	 */
	private String Url;

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
