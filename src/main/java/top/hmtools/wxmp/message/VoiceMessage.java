package top.hmtools.wxmp.message;

public class VoiceMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7630075908133536334L;

	/**
	 * 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	private String MediaId;
	
	/**
	 * 语音格式，如amr，speex等
	 */
	private String Format;

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getFormat() {
		return Format;
	}

	public void setFormat(String format) {
		Format = format;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "VoiceMessage [MediaId=" + MediaId + ", Format=" + Format + ", ToUserName=" + ToUserName
				+ ", FromUserName=" + FromUserName + ", CreateTime=" + CreateTime + ", MsgType=" + MsgType + ", MsgId="
				+ MsgId + "]";
	}
	
	
}
