package top.hmtools.wxmp;

import top.hmtools.wxmp.accessToken.AccessTokenTools;
import top.hmtools.wxmp.httpclient.HttpAdapter;
import top.hmtools.wxmp.mediaAndMaterial.MediaTools;
import top.hmtools.wxmp.menu.MenuTools;
import top.hmtools.wxmp.user.UserTagsTools;

/**
 * 微信公众号接口统一入口工具
 * @author Hybomyth
 *
 */
public class WxmpTools {
	
	private static MediaTools mediaTools;
	
	private static MenuTools menuTools;
	
	private static UserTagsTools userTagsTools;

	/**
	 * 获取 AccessTokenTools 对象实例，可对access Token进行操作
	 * @return
	 */
	public static AccessTokenTools getAccessTokenTools(){
		return HttpAdapter.getAccessTokenTools();
	}
	
	/**
	 * 线程安全的获取单例的 MediaTools
	 * @return
	 */
	public static MediaTools getMediaTools(){
		if(mediaTools == null){
			synchronized (MediaTools.class) {
				if(mediaTools == null){
					mediaTools = new MediaTools();
				}
			}
		}
		return mediaTools;
	}

	/**
	 * 线程安全的获取单例的 MenuTools
	 * @return
	 */
	public static MenuTools getMenuTools(){
		if(menuTools == null){
			synchronized (MenuTools.class) {
				if(menuTools == null){
					menuTools = new MenuTools();
				}
			}
		}
		return menuTools;
	}

	/**
	 * 线程安全的获取单例的 UserTagsTools
	 * @return
	 */
	public static UserTagsTools getUserTagsTools(){
		if(userTagsTools == null){
			synchronized (UserTagsTools.class) {
				if(userTagsTools == null){
					userTagsTools = new UserTagsTools();
				}
			}
		}
		return userTagsTools;
	}
}
