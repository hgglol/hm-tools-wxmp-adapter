package top.hmtools.wxmp.user;

import java.io.Serializable;

/**
 * 用户、公众号标签实体类
 * @author HyboJ
 *
 */
public class TagsBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5519101931211889436L;

	protected String name;
	
	private int id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "TagsBean [name=" + name + ", id=" + id + "]";
	}

	
	
}
