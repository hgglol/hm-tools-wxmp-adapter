package top.hmtools.wxmp.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.httpclient.HttpAdapter;

/**
 * 用户标签管理
 * @author HyboJ
 *
 */
public class UserTagsTools {

	final Logger logger = LoggerFactory.getLogger(UserTagsTools.class);
	
	private String createTagsUri = "cgi-bin/tags/create";
	
	/**
	 * 创建一个标签
	 * @param tagsParamBean
	 * @return
	 */
	public TagsBean createTags(TagsParamBean tagsParamBean){
		TagsParamBean result = HttpAdapter.doPostJson(this.createTagsUri, tagsParamBean,TagsParamBean.class);
		return result==null?null:result.getTag();
	}
}
