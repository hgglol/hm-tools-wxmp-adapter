package top.hmtools.wxmp.user;

/**
 * 请求用户标签相关微信公众号接口时的参数实体类
 * @author HyboJ
 *
 */
public class TagsParamBean {

	private TagsBean tag;

	public TagsBean getTag() {
		return tag;
	}

	public void setTag(TagsBean tag) {
		this.tag = tag;
	}
	
	
}
