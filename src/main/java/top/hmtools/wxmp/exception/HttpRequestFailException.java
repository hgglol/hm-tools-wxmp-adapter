package top.hmtools.wxmp.exception;

/**
 * 请求微信服务器失败后的运行时异常
 * @author Hybomyth
 *
 */
public class HttpRequestFailException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -84260494719390699L;
	private int httpStatusCode;
	
	public HttpRequestFailException() {
		super();
	}
	
	public HttpRequestFailException(String msg) {
		super(msg);
	}
	
	public HttpRequestFailException(String msg,int httpStatusCode) {
		super(msg);
		this.httpStatusCode=httpStatusCode;
	}

	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	
	
}
