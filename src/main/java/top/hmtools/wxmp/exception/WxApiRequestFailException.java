package top.hmtools.wxmp.exception;

import top.hmtools.wxmp.httpclient.ErrcodeBean;

/**
 * 请求微信服务器失败后的运行时异常
 * @author Hybomyth
 *
 */
public class WxApiRequestFailException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5417970756817280030L;
	private ErrcodeBean errcodeBean;
	
	public WxApiRequestFailException() {
		super();
	}
	
	public WxApiRequestFailException(String msg) {
		super(msg);
	}
	
	public WxApiRequestFailException(String msg,ErrcodeBean errcodeBean) {
		super(msg+","+(errcodeBean.getErrcode()==0?"":errcodeBean.getErrcode())+":"+(errcodeBean.getErrmsg()==null?"":errcodeBean.getErrmsg()));
		this.errcodeBean=errcodeBean;
	}

	public ErrcodeBean getErrcodeBean() {
		return errcodeBean;
	}

	public void setErrcodeBean(ErrcodeBean errcodeBean) {
		this.errcodeBean = errcodeBean;
	}
	
	
}
