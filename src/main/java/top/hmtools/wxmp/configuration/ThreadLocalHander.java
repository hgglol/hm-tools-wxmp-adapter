package top.hmtools.wxmp.configuration;

/**
 * 用于优雅读写配置信息的threadLocal工具
 * @author Hybomyth
 *
 */
public class ThreadLocalHander {

	private static ThreadLocal<WxmpConfiguration> wxmpConfigTL = new ThreadLocal<>();
	
	/**
	 * 设置配置信息到threadLocal
	 * @param wxmpConfiguration
	 */
	public static void setWxmpConfiguration(WxmpConfiguration wxmpConfiguration){
		wxmpConfigTL.set(wxmpConfiguration);
	}
	
	/**
	 * 从threadLocal中获取配置信息
	 * @return
	 */
	public static WxmpConfiguration getWxmpConfiguration(){
		return wxmpConfigTL.get();
	}
}
