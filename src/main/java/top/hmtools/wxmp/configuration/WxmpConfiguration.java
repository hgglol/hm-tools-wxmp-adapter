package top.hmtools.wxmp.configuration;

import top.hmtools.wxmp.UrlServer.EUrlServer;

/**
 * 全局配置信息类
 * @author Hybomyth
 *
 */
public class WxmpConfiguration {

	private String appid;

	private String appsecret;
	
	/**
	 * 收、发数据时的字符编码，缺省 utf-8
	 */
	private String charset;
	
	/**
	 * 微信服务器地址
	 */
	private EUrlServer eUrlServer;

	public String getAppid() {
		return appid;
	}

	public WxmpConfiguration setAppid(String appid) {
		this.appid = appid;
		return this;
	}

	public String getAppsecret() {
		return appsecret;
	}

	public WxmpConfiguration setAppsecret(String appsecret) {
		this.appsecret = appsecret;
		return this;
	}
	
	/**
	 * 获取微信服务器地址
	 * @return
	 */
	public EUrlServer geteUrlServer() {
		if(this.eUrlServer==null){
			this.eUrlServer=EUrlServer.api;
		}
		return eUrlServer;
	}

	/**
	 * 设置微信服务器地址
	 * @param eUrlServer
	 */
	public WxmpConfiguration seteUrlServer(EUrlServer eUrlServer) {
		this.eUrlServer = eUrlServer;
		return this;
	}

	/**
	 * 将当前本对象实例存入ThreadLocal
	 * @return
	 */
	public WxmpConfiguration saveToThreadLocal(){
		ThreadLocalHander.setWxmpConfiguration(this);
		return this;
	}

	/**
	 * 收、发数据时的字符编码，缺省 utf-8
	 * @return
	 */
	public String getCharset() {
		return charset;
	}

	/**
	 * 收、发数据时的字符编码，缺省 utf-8
	 * @param charset
	 */
	public WxmpConfiguration setCharset(String charset) {
		this.charset = charset;
		return this;
	}

	@Override
	public String toString() {
		return "WxmpConfiguration [appid=" + appid + ", appsecret=" + appsecret + ", charset=" + charset
				+ ", eUrlServer=" + eUrlServer + "]";
	}

	
}
