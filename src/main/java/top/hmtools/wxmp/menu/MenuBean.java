package top.hmtools.wxmp.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4412389461451717600L;
	
	private List<ButtonBean> button = new ArrayList<>();
	
	/**
	 * 添加一个按钮（一级菜单），最多只能添加3个，多出的会被忽略
	 * @param buttonBeans
	 */
	public void addButton(ButtonBean...buttonBeans ){
		for(ButtonBean btn:buttonBeans){
			if(this.button.size()<=3){
				this.button.add(btn);
			}
		}
	}

	/**
	 * 获取按钮（一级菜单）
	 * @return
	 */
	public List<ButtonBean> getButton() {
		return button;
	}

	/**
	 * 设置按钮（一级菜单）
	 * @param button
	 */
	public void setButton(List<ButtonBean> buttons) {
		if(buttons.size()>3){
			this.button.addAll(buttons.subList(0, 3));
		}else{
			this.button=buttons;
		}
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "MenuBean [button=" + button + "]";
	}

}
