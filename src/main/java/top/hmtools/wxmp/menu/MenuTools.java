package top.hmtools.wxmp.menu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

import top.hmtools.wxmp.httpclient.ErrcodeBean;
import top.hmtools.wxmp.httpclient.HttpAdapter;

/**
 * 微信公众号自定义菜单相关工具
 * @author Hybomyth
 *
 */
public class MenuTools {
	
	final Logger logger = LoggerFactory.getLogger(MenuTools.class);
	
	private String getMenuUri = "cgi-bin/menu/get";
	
	private String createMenuUri = "cgi-bin/menu/create";
	
	private String deleteMenuUri = "cgi-bin/menu/delete";
	
	/**
	 * 创建自定义菜单
	 * @param menuBean
	 * @return
	 */
	public boolean createMenu(MenuBean menuBean){
		boolean result = false;
		ErrcodeBean errcodeBean = HttpAdapter.doPostJson(this.createMenuUri , menuBean,ErrcodeBean.class);
		result = errcodeBean != null && errcodeBean.getErrcode() == 0;
        return result;
	}

	/**
	 * 获取自定义菜单信息
	 * @return
	 */
	public MenuBean getMenu(){
		MenuBean result = null;
		JSONObject jsonObject = HttpAdapter.doGetJson(this.getMenuUri, null,JSONObject.class);
		if(jsonObject!=null){
			result = jsonObject.getJSONObject("menu").toJavaObject(MenuBean.class);
		}
		return result;
	}
	
	/**
	 * 删除全部自定义菜单信息
	 * @return
	 */
	public boolean deleteMenu(){
		boolean result = false;
		ErrcodeBean errcodeBean = HttpAdapter.doGetJson(this.deleteMenuUri, null, ErrcodeBean.class);
		result = errcodeBean != null && errcodeBean.getErrcode() == 0;
        return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
