package top.hmtools.wxmp.accessToken;

import java.io.Serializable;
import java.util.Date;

/**
 * 记录微信access_token信息的实体类
 * @author Hybomyth
 *
 */
public class ATBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2685290436069468970L;

	/**
	 * 第三方用户唯一凭证
	 */
	private String appid;
	
	/**
	 * 第三方用户唯一凭证密钥，即appsecret
	 */
	private String secret;
	
	/**
	 * 获取到的凭证
	 */
	private String access_token;
	
	/**
	 * 上次修改数据时间戳
	 */
	private Date lastModifyDatetime;
	
	/**
	 * 凭证有效时间，单位：秒
	 */
	private long expires_in;

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public Date getLastModifyDatetime() {
		return lastModifyDatetime;
	}

	public void setLastModifyDatetime(Date lastModifyDatetime) {
		this.lastModifyDatetime = lastModifyDatetime;
	}

	public long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ATBean [appid=" + appid + ", secret=" + secret + ", access_token=" + access_token
				+ ", lastModifyDatetime=" + lastModifyDatetime + ", expires_in=" + expires_in + "]";
	}
	
	
}
