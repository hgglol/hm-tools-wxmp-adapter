#### 获取微信accessToken的工具
- 因微信公众号的accessToken具有单个有有效时长，且单位时间内有获取次数限制，故为了充分使用accessToken资源开发此工具。
- 主要原理：将每次获取的accessToken缓存到本地磁盘，直到accessToken失效时，再自动获取。
- 工具主要类是：com.xqcpjy.wxTools.accessToken.AccessTokenTools